# React Native 开发规范

- 推荐使用 webstorm
- 开始写代码之前请先配置好 prettier
  自动格式化插件，在代码保存的时候可以立马格式化当前代码文件
- 推荐使用 ES6 语法
- 提交代码建议使用 git commit 命令，这样才能看到 eslint 的代码错误提示
- 正常情况请不要跳过提交代码的自动检查

## 内容目录

1. [命名规范](#命名规范)
2. [代码格式](#代码格式)
3. [模块定义](#模块定义)
4. [模块引用](#模块引用)
5. [组件样式](#组件样式)
6. [静态资源](#静态资源)
   <!--7. [组件的复用](#组件的复用)-->
   <!--8. [组件的跨端方案](#组件的跨端方案)-->

## 命名规范

文件扩展名：React 模块使用 `.tsx` 命名，非 React 模块使用 `.ts` 命名
，目录/文件使用 烤串命名（小写字母+中划线）

- 模块和文件夹命名

```
// 不推荐 ❌
目录 securityCenterScreen
文件 apiConfig.ts

// 推荐 ✅
目录 security-center-screen
文件 api-config.ts
```

- 组件、视图 使用 帕斯卡命名（大写字母开头的驼峰式命名）

```
// 不推荐 ❌
// 组件/视图 定义
export class textField extends Component
export class login_screen extends Component

// 推荐 ✅
// 组件/视图 定义命名
export class TextField extends Component
export class LoginScreen extends Component
```

- 组件样式统一使用对象的方式声明 使用 Style 结尾对象命名
- 需要声明 Style 类型，如 ViewStyle ImageStyle TextStyle

```
// 不推荐 ❌
const header: ViewStyle = {
  backgroundColor: "#fff",
  alignItems: "center"
}

// 推荐 ✅
const headerStyle: ViewStyle = {
  backgroundColor: "#fff",
  alignItems: "center"
}
```

- Props 属性命名（使用骆驼式风格）

```
// 不推荐 ❌
<Foo
  UserName="hello"
  phone_number={12345678}
/>

// 推荐 ✅
<Foo
  userName="hello"
  phoneNumber={12345678}
/>
```

- 如果属性值为 true, 可以直接省略

```
// 不推荐 ❌
<Foo
  hidden={true}
/>

// 推荐 ✅
<Foo hidden />
```

## 代码格式

> 如果你配置好了 `prettier` 它可以在保存代码的时候自动格式化成规范要求的样子

- 统一使用双引号，结尾不加引号

```
// 不推荐 ❌
<Foo bar='bar' />
import { Image } from 'react-native'
const str = 'hello'

// 推荐 ✅
<Foo bar="bar" />
import { Image } from "react-native"
const str = "hello"

```

- 代码对齐

```
// 不推荐 ❌
<Foo superLongParam="bar"
     anotherSuperLongParam="baz" />

// 推荐 ✅，有多行属性 闭合标签需要独立一行
<Foo
  superLongParam="bar"
  anotherSuperLongParam="baz"
/>

// 推荐 ✅，如果属性较少可以写成一行
<Foo title="bar" />
```

- 空格，单标签闭合需要再 / 前面加一个空格

```
// 不推荐 ❌
<Foo/>

// 不推荐 ❌
<Foo           />

// 不推荐 ❌
<Foo
 />

// 推荐 ✅
<Foo />
```

- ES6 import 引入，花括号两边要空一格，属性之间逗号后面空一格

```
// 不推荐 ❌
import {Button,Image} from "react-native"

// 推荐 ✅
import { Button, Image } from "react-native"
```

## 模块定义

- 如果模块有内部状态或者是 `refs` 推荐使用 class extends React.Component

```
// 不推荐 ❌
const Listing = React.createClass({
  render() {
    return <View>{this.state.hello}</View>
  }
})
// 推荐 ✅
class Listing extends React.Component {
  // ...
  render() {
    return <View>{this.state.hello}</View>
  }
}
```

- 如果你的模块没有状态或是没有引用 refs， 推荐使用普通函数（非箭头函数）

```
// 不推荐 ❌
class Listing extends React.Component {
  render() {
    return <div>{this.props.hello}</div>
  }
}

const Listing = ({ hello }) => (
  <View>{hello}</View>
)

// 推荐 ✅
function Listing({ hello }) {
  return <View>{hello}</View>
}
```

## 模块引用

- 组件、视图、Utils 工具类 统一导出到 index.ts，引入的时候分别从
  /components/index/ts 、/screens/index.ts、/utils/index.ts 中按需引入

```
// /components/index.ts
export * from "./auth-component"
export * from "./banner"
export * from "./button"
export * from "./bullet-item"

// /screens/index.ts
export * from "./bet-record-screen"
export * from "./entertainment-screen"
export * from "./login-screen"

// 引入方式（根据实际相对路径）
import { Button, Banner } from "../components"
import { LoginScreen } from "../screens"
import { px2dp } from "../utils"
```

## 组件样式

- Style 样式命名

```
// 不推荐 ❌
const headerTopStyle = {
  flexDirection: "row",
  justifyContent: "flex-end",
  padding: px2dp(10)
}

// 推荐 ✅
// Style对象（需要声明对应的组件类型，如 ViewStyle TextStyle）
const headerTopStyle: ViewStyle = {
  flexDirection: "row",
  justifyContent: "flex-end",
  padding: px2dp(10)
}
```

- 如果样式过多，需要在组件目录下创建 styles.ts 存放样式对象

```
// styles.ts

export const headerTopStyle: ViewStyle = {
  flexDirection: "row",
  justifyContent: "flex-end",
  padding: px2dp(10)
}

// 引入组件中的 styles.ts
import { headerTopStyle } from "./styles.ts"
```

- 稍微复杂的组件，为了方便维护，也可以写到一个对象的下面

```
// 记得声明类型
interface boxStyleProps {
  header?: ViewStyle,
  container?: ViewStyle,
  footer?: ViewStyle
}

const boxStyle:boxStyleProps = {
  header: {
    height: px2dp(100),
    backgroundColor: '#fff'
  },
  container: {
    height: px2dp(200)
  },
  footer: {
    height: 30,
    backgroundColor: '#000'
  }
}
```

- 样式的复用（如有可复用样式，比如颜色、间距等样式可以写到 theme 中对应的文件中）
- 如果是 对象 使用扩展运算符合并，单个样式可以直接用属性接收

```
// 引用公用样式
import { color } from "../theme"
const headerTextStyle: TextStyle = {
  color: color.backgroundColor
}

// 合并样式
const btnStyle: ViewStyle = {
  width: px2dp(100),
  height: px2dp(40),
  borderRadius: px2dp(4),
  color: "#000"
}

const activeBtnStyle: ViewStyle = {
  ...btnStyle,
  backgroundColor: "#000",
  color: "#fff"
}

// 合并后的样式（需要注意扩展运算符合并其他样式的时候要注意顺序，放在最前面，才能被覆盖，放在最后面会覆盖前面的样式）
{
  width: px2dp(100),
  height: px2dp(40),
  borderRadius: px2dp(4),
  backgroundColor: "#000",
  color: "#fff"
}
```

## 静态资源

> 为了方便维护，图片、图标、字体存放在 app/assets 中， 统一定义在 目录下的
> index.ts 中

- 资源的使用

```
import { images } from "../../assets"

<Image source={images.entertainment.entry.live.ag} />
```
