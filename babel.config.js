module.exports = function(api) {
  let isH5Dev = api.cache(() => process.env.NODE_ENV === "development")
  let plugins = [
    [
      "transform-inline-environment-variables",
      {
        include: ["NODE_ENV", "API"]
      }
    ],
    [
      "@babel/plugin-proposal-decorators",
      {
        legacy: true
      }
    ],
    ["@babel/plugin-proposal-optional-catch-binding"],
    ["@babel/plugin-transform-runtime"],
    [
      "module-resolver",
      {
        root: ["./"],
        alias: {
          "@": "./app",
          consts: "./app/consts",
          screens: "./app/screens"
        },
        extensions: [".js", ".ts", ".jsx", ".tsx", ".ios.js", ".android.js"]
      }
    ]
  ]

  if (isH5Dev) {
    return {
      presets: ["module:metro-react-native-babel-preset", "react"],
      env: {
        production: {}
      },
      plugins: [
        ...plugins,
        ["import", { libraryName: "antd-mobile", style: "css" }],
        [
          "module-resolver",
          {
            alias: {
              "^react-native$": "react-native-web"
            }
          }
        ]
      ]
    }
  } else {
    return {
      presets: ["module:metro-react-native-babel-preset"],
      env: {
        production: {}
      },
      plugins: [...plugins, ["import", { libraryName: "@ant-design/react-native" }]]
    }
  }
}
