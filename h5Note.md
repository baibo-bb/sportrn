<!--
 * @Descripttion: 对 RN转h5的一些描述
 * @Author: Lucia
 * @Date: 2019-08-06 19:27:38
 * @LastEditors: Lucia
 * @LastEditTime: 2019-08-09 20:03:03
 * @msg: 未成功转换之前，持续更新
 -->

# H5

## 启动

- 同步 dev 分支的配置后，执行命令 yarn install
- 执行命令 yarn run web，打开 h5 网页

## webpack 配置文件

```
./config/
./tsconfig.json
./babel.config.js
```

## React-Native 转 h5 的解决方案：

### react-native-web：

      支持把 React Native 编写的项目转换成 Web 应用，实现了在不修改React Native代码的情况下渲染在浏览器里。这个转换方案目前已用在 Twitter、Flipkart、Uber 等项目中。

### react-native-web 实现原理：

- 在 webpack 构建用于运行在浏览器的代码时，把 React Native 的导入路径替换为 react-native-web 的倒入路径；
- 在 react-native-web 内部已经实现了与 React Native 名称一致的组件，替换引入的时候，就把 React Native 的 API 映射为浏览器所支持的 API。

### 完成这个工作需要掌握的：

- React 和 React-native；
- React Navigation 和 react-router;
- Mobx；
- react-native-web RN 转 H5 插件；
- webpack 项目构建工具；
- typesrcipt -->因为我们项目的脚手架 inigte-cli 使用 typesrcipt，所以必须去了解，以读懂和翻译原本的页面。

### RN 转 h5 需要注意处理的点：

- 把 React Native 引用专向 react-native-web，react-native-web 已经实现的组件可以复用，没有实现的组件需要自己重写，但绝大部分组件已经实现；

- 转换路由，React Native 的路径是使用 React Navigation 实现，转 web 要翻译成 react-router；

- 第三方 RN 独有的插件处理：
  - React Native 使用 react-native-gesture-handler 做手势处理，h5 不适用;
  - React Native 使用 react-native-elements 这一个 UI 库，h5 不适用;
  - React Native 使用 react-native-localize 处理多语言，h5 不适用.

### React-Native 转 h5 的历程：

- 了解转换方案 react-native-web 的实现原理；
- 配置脚手架；
- 熟悉 React Navigation、Mobx 和在用的各种 RN 的第三方插件；
- 将 React Navigation 翻译为 react-router。
- 自己实现项目中 react-native-web 未实现的 react-native 组件

## **心得**：

- 1.用 react-native-web 去转换已成形的 React Native 项目，复杂度相对要高很多；
- 2.需要熟悉原本项目所使用的所有技术和插件，找出所有仅适用于 React Native 的实现方式去做转换；
- 3.最好的方式就是，从开发最开始的设计，必须三端同时考虑，尽量提高组件、代码的复用性。

## react-native 用到项目中且 react-native-web 未实现的组件

- Modal 引自 react-native-modal antd 解决
- Alert 引自 react-native antd 解决

- RN 第三方插件库：react-native-elements 里用的 Icon Button，
  解决：可以使用`ant design Mobile RN` 和 `ant design Mobile`来处理差异，Image 不支持
  问题点：Button 在 mobile-web 是点击是 onClick 在 app 是 onPress

- react-native-localize 和 react-native-i18n 处理多语

- 轮播图插件 react-native-swiper，
  解决：react-native-web-swiper

- react-native-keychain 除了工具函数，其他地方没有用到,h5 应该用不到，不会导致报错

- @react-native-community/async-storage 缓存处理，使用 react native web  支持的 AsyncStorage

- 不用像素的图片加载问题 react-native-web-image-loader

## react-native-elements

[web 端文档](https://google.github.io/material-design-icons/)
